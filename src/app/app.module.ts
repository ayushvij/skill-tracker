import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';
import {CdkTextareaAutosize} from '@angular/cdk/text-field';
import {MatDividerModule} from '@angular/material/divider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatSliderModule} from '@angular/material/slider';
import {MatTableModule} from '@angular/material/table';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatCardModule} from '@angular/material/card';
import { HomeTableComponent } from './home/home.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { SkillTableComponent } from './skill-table/skill-table.component';
import { EmployeeEntryComponent } from './employeeEntry/employeeEntry.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes  } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { EmployeeEditComponent } from './employeeEdit/employeeEdit.component';
import { TrackComponent } from './track/track.component';
import { MatDialogModule } from '@angular/material/dialog';
import { SkillEditComponent} from './skillEdit/skillEdit.component';
import { HeaderComponent } from './header/header.component';
import {MatIconModule} from '@angular/material/icon';
import { OwnSkillComponent } from './ownSkill/ownSkill.component';
import {MatListModule} from '@angular/material/list';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatRadioModule} from '@angular/material/radio';
import { EmployeeViewComponent } from './employeeView/employeeView.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './auth.guard';
import { AuthService } from './services/auth.service';
import { MatConfirmDialogComponent } from './mat-confirm-dialog/mat-confirm-dialog.component';
import {MatTooltipModule} from '@angular/material/tooltip';
// import * as CanvasJS from './canvasjs.min';
import { FusionChartsModule } from 'angular-fusioncharts';

// Import FusionCharts library
import * as FusionCharts from 'fusioncharts';

// Load FusionCharts Individual Charts
import * as Charts from 'fusioncharts/fusioncharts.charts';

// Use fcRoot function to inject FusionCharts library, and the modules you want to use
FusionChartsModule.fcRoot(FusionCharts, Charts );
import {animate, state, style, transition, trigger} from '@angular/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeTableComponent,
    SkillTableComponent,
    EmployeeEntryComponent,
    EmployeeEditComponent,
    SkillEditComponent,
    HeaderComponent,
    EmployeeViewComponent,
    LoginComponent,
    RegisterComponent,
    TrackComponent,
    OwnSkillComponent,
    // WrongLoginComponent,
    MatConfirmDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MatSliderModule,
    MatTableModule,
    FlexLayoutModule,
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    ReactiveFormsModule,
    // RouterModule.forRoot(appRoute),
    HttpClientModule,
    MatButtonModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
       [
        { path: '', component: LoginComponent},
      ]
    ),
    MatCheckboxModule,
    Ng2SearchPipeModule,
    MatDialogModule,
    MatIconModule,
    MatListModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule,
    MatRadioModule,
    FusionChartsModule,
    MatExpansionModule,
    MatProgressSpinnerModule
  ],
  exports: [ SkillEditComponent, EmployeeEditComponent, EmployeeViewComponent, TrackComponent],
  entryComponents: [SkillEditComponent, EmployeeEditComponent, EmployeeViewComponent, TrackComponent, MatConfirmDialogComponent],
  providers: [AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
