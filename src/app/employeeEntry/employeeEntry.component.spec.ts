import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { BrowserModule, By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { HeaderComponent } from '../header/header.component';
import { EmployeeEntryComponent } from './employeeEntry.component';
import { AngularMaterialModule } from '../shared/modules/angular-material/angular-material.module';
describe('EmployeeEntryComponent', () => {
    let comp: EmployeeEntryComponent;
    let fixture: ComponentFixture<EmployeeEntryComponent>;
    let de: DebugElement;
    let el: HTMLElement;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                EmployeeEntryComponent,
                HeaderComponent,
            ],
            imports: [
               AngularMaterialModule,
               RouterTestingModule,
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(EmployeeEntryComponent);

            comp = fixture.componentInstance; // ContactComponent test instance
            // query for the title <h1> by CSS element selector
            de = fixture.debugElement.query(By.css('form'));
            el = de.nativeElement;
        });
    }));
    it('should create', () => {
    expect(comp).toBeTruthy();
  });

    it(`should have as text 'add page'`, async(() => {
        expect(comp.text).toEqual('add page');
    }));

    it(`should set submitted to true`, async(() => {
        comp.onSubmit();
        expect(comp.submitted).toBeTruthy();
    }));

    it(`should call the onSubmit method`, async(() => {
        spyOn(comp, 'onSubmit');
        el = fixture.debugElement.query(By.css('button')).nativeElement;
        el.click();
        expect(comp.onSubmit).toHaveBeenCalled();
    }));

    it(`form should be invalid`, async(() => {
        /* tslint:disable:no-string-literal */
        comp.newEmployee.controls['employeeName'].setValue('');
        comp.newEmployee.controls['employeeId'].setValue('');
        comp.newEmployee.controls['employeeNumber'].setValue('');
        comp.newEmployee.controls['employeeEmail'].setValue('');
        comp.newEmployee.controls['employeeRemark'].setValue('');
        comp.newEmployee.controls['employeeLevel1'].setValue(false);
        comp.newEmployee.controls['employeeLevel2'].setValue(false);
        comp.newEmployee.controls['employeeLevel3'].setValue(false);
        comp.newEmployee.controls['employeeStatusGreen'].setValue(false);
        comp.newEmployee.controls['employeeStatusRed'].setValue(false);
        comp.newEmployee.controls['employeeStatusBlue'].setValue(false);
        comp.newEmployee.controls['other'].setValue('');
        comp.newEmployee.controls['employeeStrength'].setValue('');
        comp.newEmployee.controls['employeeWeakness'].setValue('');
        expect(comp.newEmployee.valid).toBeFalsy();
    }));

    it(`form should be valid`, async(() => {
        /* tslint:disable:no-string-literal */
        comp.newEmployee.controls['employeeName'].setValue('Ayush');
        comp.newEmployee.controls['employeeId'].setValue('797990');
        comp.newEmployee.controls['employeeNumber'].setValue('9473649213');
        comp.newEmployee.controls['employeeEmail'].setValue('Ayus@gmail.com');
        comp.newEmployee.controls['employeeRemark'].setValue('ohk');
        comp.newEmployee.controls['employeeLevel1'].setValue(false);
        comp.newEmployee.controls['employeeLevel2'].setValue(true);
        comp.newEmployee.controls['employeeLevel3'].setValue(false);
        comp.newEmployee.controls['employeeStatusGreen'].setValue(false);
        comp.newEmployee.controls['employeeStatusRed'].setValue(true);
        comp.newEmployee.controls['employeeStatusBlue'].setValue(false);
        comp.newEmployee.controls['other'].setValue('fine');
        comp.newEmployee.controls['employeeStrength'].setValue('ohkkk');
        comp.newEmployee.controls['employeeWeakness'].setValue('no');
        expect(comp.newEmployee.valid).toBeTruthy();
    }));
});
