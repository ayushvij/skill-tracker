import { Component, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Skill } from '../model/skill';
import { AuthService } from '../services/auth.service';
import { EmployeeService } from '../services/employee.service';
import { SkillService } from '../services/skill.service';
import { Employee } from '../model/employee';
import { SkillRating } from '../model/skillrating';

@Component({
    selector: 'app-employee-entry',
    templateUrl: './employeeEntry.component.html',
    styleUrls: ['./employeeEntry.component.css']
})


export class EmployeeEntryComponent implements OnInit {
    myEmployee: Employee;
    text = 'add page';
    submitted = false;
    employeeList: any;
    mySkills: any;
    myGender: string;
    gender: string[] = ['Male', 'Female', 'Other'];
    firstCheckBox: boolean;
    newEmployee = new FormGroup({
        employeeName: new FormControl(''),
        employeeId: new FormControl(''),
        employeeEmail: new FormControl(''),
        employeeNumber: new FormControl(''),
        employeeLevel1: new FormControl(false),
        employeeLevel2: new FormControl(false),
        employeeLevel3: new FormControl(false),
        employeeStatusGreen: new FormControl(false),
        employeeStatusRed: new FormControl(false),
        employeeStatusBlue: new FormControl(false),
        employeeRemark: new FormControl(''),
        employeeStrength: new FormControl(''),
        employeeWeakness: new FormControl(''),
        other: new FormControl(''),
    });
    max = 20;
    min = 0;
    thumbLabel = true;
    value = 0;
    ngOnInit() {
        this.skillService.viewSkills().subscribe((response) => {
            this.mySkills = response;
        });
    }
    constructor(private employeeService: EmployeeService, private skillService: SkillService,
                private router: Router, private auth: AuthService) { }
    onSubmit() {
        this.myEmployee = new Employee();
        this.myEmployee.employeeName = this.newEmployee.get('employeeName').value;
        this.myEmployee.employeeId = this.newEmployee.get('employeeId').value;
        this.myEmployee.employeeEmail = this.newEmployee.get('employeeEmail').value;
        this.myEmployee.employeeNumber = this.newEmployee.get('employeeNumber').value;
        this.myEmployee.employeeLevel1 = this.newEmployee.get('employeeLevel1').value;
        this.myEmployee.employeeLevel2 = this.newEmployee.get('employeeLevel2').value;
        this.myEmployee.employeeLevel3 = this.newEmployee.get('employeeLevel3').value;
        this.myEmployee.employeeStatusGreen = this.newEmployee.get('employeeStatusGreen').value;
        if (this.myEmployee.employeeStatusGreen == null) {
            this.myEmployee.employeeStatusGreen = false;
        }
        this.myEmployee.employeeStatusBlue = this.newEmployee.get('employeeStatusBlue').value;
        if (this.myEmployee.employeeStatusBlue == null) {
            this.myEmployee.employeeStatusBlue = false;
        }
        this.myEmployee.employeeStatusRed = this.newEmployee.get('employeeStatusRed').value;
        if (this.myEmployee.employeeStatusRed == null) {
            this.myEmployee.employeeStatusRed = false;
        }
        this.myEmployee.employeeStrength = this.newEmployee.get('employeeStrength').value;
        this.myEmployee.employeeRemark = this.newEmployee.get('employeeRemark').value;
        this.myEmployee.employeeWeakness = this.newEmployee.get('employeeWeakness').value;
        this.myEmployee.mygender = this.myGender;
        this.myEmployee.other = this.newEmployee.get('other').value;
        const mySkillsfresh = [];
        for (let i = 0, k = 0; i < this.mySkills.length; i++) {
            if ((this.mySkills[i].rating) != null) {
                mySkillsfresh[k] = this.mySkills[i];
                k++;
            }
        }
        this.submitted = true;
        this.myEmployee.skillRating = mySkillsfresh;
        this.employeeService.saveData(this.myEmployee);
        this.router.navigate(['home']);
        this.auth.setLoggedIn(true);
        // location.reload();
    }
    onclicklevel(tring: any, checkbox: HTMLInputElement) {
        if ((tring === 'level1') && (checkbox.checked === true)) {
            (<HTMLInputElement> document.getElementById('level2')).disabled = true;
            (<HTMLInputElement> document.getElementById('level3')).disabled = true;
        }
        if ((tring === 'level1') && (checkbox.checked === false)) {
            (<HTMLInputElement> document.getElementById('level2')).disabled = false;
            (<HTMLInputElement> document.getElementById('level3')).disabled = false;
        }
        if ((tring === 'level2') && (checkbox.checked === true)) {
            (<HTMLInputElement> document.getElementById('level1')).disabled = true;
            (<HTMLInputElement> document.getElementById('level3')).disabled = true;
        }
        if ((tring === 'level2') && (checkbox.checked === false)) {
            ( <HTMLInputElement> document.getElementById('level1')).disabled = false;
            ( <HTMLInputElement> document.getElementById( 'level3' )).disabled = false;
        }
        if ((tring === 'level3') && (checkbox.checked === true)) {
            ( <HTMLInputElement> document.getElementById('level1')).disabled = true;
            ( <HTMLInputElement> document.getElementById('level2')).disabled = true;
        }
        if ((tring === 'level3') && (checkbox.checked === false)) {
            ( <HTMLInputElement> document.getElementById('level1')).disabled = false;
            ( <HTMLInputElement> document.getElementById('level2')).disabled = false;
        }
    }
    onclick(tring: any, checkbox: HTMLInputElement) {
        if ((tring === 'blue') && (checkbox.checked === true)) {
            (<HTMLInputElement> document.getElementById('green')).disabled = true;
            (<HTMLInputElement> document.getElementById('red')).disabled = true;
        }
        if ((tring === 'blue') && (checkbox.checked === false)) {
            (<HTMLInputElement> document.getElementById('green')).disabled = false;
            (<HTMLInputElement> document.getElementById('red')).disabled = false;
        }
        if ((tring === 'green') && (checkbox.checked === true)) {
            (<HTMLInputElement> document.getElementById('blue')).disabled = true;
            (<HTMLInputElement> document.getElementById('red')).disabled = true;
        }
        if ((tring === 'green') && (checkbox.checked === false)) {
            (<HTMLInputElement> document.getElementById('blue')).disabled = false;
            (<HTMLInputElement> document.getElementById('red')).disabled = false;
        }
        if ((tring === 'red') && (checkbox.checked === true)) {
            (<HTMLInputElement> document.getElementById('green')).disabled = true;
            (<HTMLInputElement> document.getElementById('blue')).disabled = true;
        }
        if ((tring === 'red') && (checkbox.checked === false)) {
            (<HTMLInputElement> document.getElementById('green')).disabled = false;
            (<HTMLInputElement> document.getElementById('blue')).disabled = false;
        }
    }
}
