import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SkillService } from '../services/skill.service';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
    selector: 'app-edit',
    templateUrl: './skillEdit.component.html',

})

export class SkillEditComponent implements OnInit {
    someValue = false;
    newSkillsupdate = new FormGroup({
        skillId: new FormControl(''),
        skillName: new FormControl(''),
    });
    constructor(private router: Router, private skillService: SkillService,
                public dialogRef: MatDialogRef < SkillEditComponent >,
                @Inject(MAT_DIALOG_DATA) public data: any) {
    }
    ngOnInit() {
        this.skillService.findSkillbyID(this.data.skillId).subscribe((response) => {
            /* tslint:disable:no-string-literal */
         this.newSkillsupdate.controls['skillId'].setValue(response['skillId']);
         this.newSkillsupdate.controls['skillName'].setValue(response['skillName']);
           /* tslint:enable:no-string-literal */
        });
    }
    onSubmit(skillId) {
        this.skillService.saveData(this.newSkillsupdate.value).subscribe((response) => {
            // console.log(response);
        });
    }
     closeDialog() {
        this.dialogRef.close(this.someValue);
    }
}


