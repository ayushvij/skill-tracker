import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Skill } from '../model/skill';
@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private http: HttpClient) {

  }
  saveData = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      'http://localhost:8080/api/skill',
      data, { headers });
  }
  viewSkills = () => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get(
      'http://localhost:8080/api/skill', { headers }
    );
  }
  deleteSkills = function(data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      'http://localhost:8080/api/deleteskills',
      { skillId: data }, { headers });
  };

  findSkill = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    this.http.post(
      'http://localhost:8080/api/skill',
      data, { headers });
  }

  findSkillbyID = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get(
      'http://localhost:8080/api/skillfindSkill/' + data,
       { headers });
  }
   saveDatanew = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      'http://localhost:8080/api/employeeskillsave',
      data, { headers })
      .subscribe((res) => {
      });
  }

  saveDatas = (skill: Skill) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post<Skill>(
      'http://localhost:8080/api/skill',
      skill, { headers })

      .subscribe((res) => {
        // console.log("After response")
        // console.log(res);
      });

  }
}
