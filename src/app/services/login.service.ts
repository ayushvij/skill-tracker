import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class LoginService {

    constructor(private http: HttpClient) {
    }
    addLogins = (data) => {
        const headers = new HttpHeaders();
        headers.append('Content-Type', 'application/json');
        return this.http.post(
            'http://localhost:8080/api/login',
            data, { headers })
             .subscribe((res) => {
      });
}

findLogins = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get(
      'http://localhost:8080/api/findLogin/' + data,
       { headers });
  }
}
