import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) {

  }
  saveData = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      'http://localhost:8080/api/employeesave',
      data, { headers })
      .subscribe((res) => {
      });
  }

  getData = () => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    // return [{
    //   'employeeId': '123', 'employeeName': 'Vij', 'employeeEmail': 'emamil', 'employeeNumber': 17237821,
    //   'employeeLevel1': true, 'employeeLevel2': true, 'employeeLevel3': true, 'employeeStatusGreen': null,
    //   'employeeStatusBlue': null, 'employeeStatusRed': null, 'employeeRemark': null, 'employeeStrength': null,
    //   'employeeWeakness': null, 'skillRating': [{ 'skillId': 1, 'skillName': 'HTML', 'rating': 12 },
    //   { 'skillId': 2, 'skillName': 'CSS', 'rating': 13 }]
    // },
    // {
    //   'employeeId': 124, 'employeeName': 'Ayush', 'employeeEmail': 'dilip@#gm', 'employeeNumber': 97898902,
    //   'employeeLevel1': true, 'employeeLevel2': true, 'employeeLevel3': true, 'employeeStatusGreen': null,
    //   'employeeStatusBlue': null, 'employeeStatusRed': null, 'employeeRemark': null, 'employeeStrength': null,
    //   'employeeWeakness': null, 'skillRating': [{ 'skillId': 3, 'skillName': 'Boot', 'rating': 1 },
    //   { 'skillId': 4, 'skillName': 'Spring', 'rating': 3 }]
    // }];
    return this.http.get(
     'http://localhost:8080/api/employee',
   { headers });

  }
  deleteEmployee = function(data) {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      'http://localhost:8080/api/deleteEmployee',
      { employeeId: data }, { headers });
  };
  findEmployeebyID = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.get(
      'http://localhost:8080/api/employee/' + data,
       { headers });
  }
  saveDatass = (data) => {
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(
      'http://localhost:8080/api/employeesave',
      data, { headers });
  }

}




