import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SkillTableComponent } from './skill-table.component';
import { HeaderComponent } from '../header/header.component';
import { AngularMaterialModule } from '../shared/modules/angular-material/angular-material.module';
describe('SkillTableComponent', () => {
  let component: SkillTableComponent;
  let fixture: ComponentFixture<SkillTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillTableComponent,
      HeaderComponent, ],
      imports: [AngularMaterialModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
}
)
;