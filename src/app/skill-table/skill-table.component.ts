import { Component, EventEmitter, Output, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { SkillService } from '../services/skill.service';
import { Skill } from '../model/skill';
import { SkillEditComponent } from '../skillEdit/skillEdit.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogService } from '../services/dialog.service';

@Component({
    selector: 'app-skill-table',
    templateUrl: './skill-table.component.html',
    styleUrls: ['./skill-table.component.css']

})

export class SkillTableComponent implements OnInit {
    mySkills: any[];
    dataSource = this.mySkills;

    displayedColumns: string[] = ['Skills', 'buttons'];

    newSkills = new FormGroup({
        skillName: new FormControl(''),
    });
    constructor(private skillService: SkillService, public dialog: MatDialog,
                private dialogService: DialogService ) {

    }
    openDialog(skillId): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '25%';
        dialogConfig.data = { skillId };
        const dialogRef = this.dialog.open(SkillEditComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
            this.skillService.viewSkills().subscribe((response: any[]) => {
            this.mySkills = response;
            this.ngOnInit();
        });
        });
    }




    ngOnInit() {
        this.skillService.viewSkills().subscribe((response: any[]) => {
            this.mySkills = response;
        });
    }

    onAdd() {
        this.skillService.saveData(this.newSkills.value).subscribe((response: any[]) => {
            this.skillService.viewSkills().subscribe(( response: any[]) => {
                this.mySkills = response;
            });
        });
        this.onReset();
    }
    onReset() {
        /* tslint:disable:no-string-literal */
        this.newSkills.controls['skillName'].setValue(['']);

    }

    // onEdit(skillId)
    // {
    //     this.skillService.findSkillbyID(skillId).subscribe((response) =>
    //     {
    //       console.log(response);
    //       this.newSkills.controls['skillName'].setValue(response['skillName']);
    //     });
    // }
    onDelete(skillId) {
         this.dialogService.openConfirmDialog('Are you sure to delete this Skill?')
      .afterClosed().subscribe(res => {
        if (res) {
            this.skillService.deleteSkills(skillId).subscribe((response) => {
            this.skillService.viewSkills().subscribe((response: any[]) => {
                this.mySkills = response;
            });
        });
        }
      });
    }
}

