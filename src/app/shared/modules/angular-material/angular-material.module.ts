import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
    MatGridListModule, MatButtonModule, MatDividerModule, MatListModule,
    MatIconModule, MatLineModule, MatCardModule
} from '@angular/material';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material';
import { Routes, RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatRadioModule} from '@angular/material/radio';
import {MatSliderModule} from '@angular/material/slider';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MatGridListModule,
        MatButtonModule,
        MatDividerModule,
        MatListModule,
        MatIconModule,
        // RouterModule.forRoot([]),
        MatLineModule,
        MatCardModule,
        MatExpansionModule,
        MatDialogModule,
        FormsModule,
        MatTableModule,
        MatFormFieldModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatRadioModule,
        MatSliderModule,
        MatCheckboxModule,
        HttpClientModule,
        MatInputModule,
        BrowserAnimationsModule,
    ],
    exports: [
        MatGridListModule,
        MatButtonModule,
        MatButtonModule,
        MatDividerModule,
        MatListModule,
        MatIconModule,
        MatLineModule,
        MatCardModule,
        MatExpansionModule,
        MatDialogModule,
        FormsModule,
        MatTableModule,
        MatFormFieldModule,
        MatToolbarModule,
        ReactiveFormsModule,
        MatRadioModule,
        MatSliderModule,
        MatCheckboxModule,
        HttpClientModule,
        MatInputModule,
        BrowserAnimationsModule,
    ]
})
export class AngularMaterialModule {
 }