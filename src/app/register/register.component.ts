import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { LoginService } from '../services/login.service';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css'],
})


export class RegisterComponent {
    constructor(private router: Router,  private loginService: LoginService) { }
    employeeId: any;
    employeeId1: any;
    password: any;
    conversionEncryptOutput: string;
    newEmployee = new FormGroup({
        employeeId: new FormControl(''),
        password: new FormControl(''),
    });

    onRegister() {
        this.employeeId1 = this.employeeId.toString();
        this.conversionEncryptOutput = CryptoJS.AES.encrypt(this.employeeId1.trim(), this.password.trim()).toString();
        /* tslint:disable:no-string-literal */
        this.newEmployee.controls['employeeId'].setValue(this.employeeId);
        this.newEmployee.controls['password'].setValue(this.conversionEncryptOutput);
        this.loginService.addLogins (this.newEmployee.value);
        this.router.navigate(['login']);
}
}
