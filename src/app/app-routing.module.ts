import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkillTableComponent } from './skill-table/skill-table.component';
import { HomeTableComponent } from './home/home.component';
import {EmployeeEditComponent} from './employeeEdit/employeeEdit.component';
import {EmployeeEntryComponent} from './employeeEntry/employeeEntry.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { OwnSkillComponent } from './ownSkill/ownSkill.component';
const routes: Routes = [
  {
    path: 'add',
    component: EmployeeEntryComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'home',
    component: HomeTableComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'addSkill',
    component: SkillTableComponent,
    // canActivate: [AdminGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    // canActivate: [AdminGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
   {
    path: 'newSkill',
    component: OwnSkillComponent
  },
  { path: '', redirectTo: '/login', pathMatch: 'full'}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
