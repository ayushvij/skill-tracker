import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from '../services/employee.service';
import { SkillService } from '../services/skill.service';
import { SkillRating } from '../model/skillrating';
import { Employee } from '../model/employee';
@Component({
    selector: 'app-employee-view',
    templateUrl: './employeeView.component.html',
    styleUrls: ['./employeeView.component.css']
})


export class EmployeeViewComponent implements OnInit {
    employeeView: Employee;
     employeeList: any[];
     allSkill: any[];
     g = 0;
     employeeStatusBlue: false;
     displayedColumns: string[] = ['name', 'Associate_Id'];
     dataSource: any[];

    constructor(private router: Router,
                private employeeService: EmployeeService,
                public dialogRef: MatDialogRef <EmployeeViewComponent>,
                private skillService: SkillService, @Inject(MAT_DIALOG_DATA) public data: any) {
                }
    ngOnInit() {
        this.employeeService.findEmployeebyID(this.data.employeeId).subscribe((response: Employee ) => {
            this.employeeView = response;
            if (this.employeeView.mygender === 'Male') {
            this.g = 1;
            } else {
            this.g = 0;
            }
            this.dataSource = this.employeeView.skillRating;
        });
    }
     closeDialog() {
        this.dialogRef.close(false);
    }
}
