import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { SkillEditComponent } from '../skillEdit/skillEdit.component';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from '../services/employee.service';
import { SkillService } from '../services/skill.service';
import { SkillRating } from '../model/skillrating';
import { Employee } from '../model/employee';
import { SkillRatingNew } from '../model/skillRatingNew';
@Component({
    selector: 'app-own-skill',
    templateUrl: './ownSkill.component.html',
    styleUrls: ['./ownSkill.component.css']
})


export class OwnSkillComponent implements OnInit {
    employeeView: Employee;
    skillnew: SkillRatingNew;
    employeeList: any[];
    mySkills: any;
    uniqueSkills = [];
    allSkill: any;
    min = 0;
    thumbLabel = true;
    max = 20;
    rating = 0;
    g = 0;
    employeeStatusBlue: false;
    displayedColumns: string[] = ['Skills', 'buttons'];

    newSkills = new FormGroup({
        skillName: new FormControl(''),
    });
    // skillnew = new FormGroup({
    //     skillName: new FormControl(''),
    //     skillId: new FormControl(''),
    //     rating: new FormControl(''),
    //     employeeId: new FormControl('')
    // });
    dataSource = [];

    constructor(private router: Router,
                private employeeService: EmployeeService,
                public dialog: MatDialog,
                public dialogRef: MatDialogRef<OwnSkillComponent>,
                private skillService: SkillService, @Inject(MAT_DIALOG_DATA) public data: any) {
    }
    ngOnInit() {
        this.employeeService.findEmployeebyID(this.data.employeeId).subscribe((response: Employee) => {
            this.employeeView = response;
            this.dataSource = this.employeeView.skillRating;
        });
        this.skillService.viewSkills().subscribe((response: any) => {
            this.mySkills = response;
            /* tslint:disable:prefer-for-of */
            for (let i = 0, k = 0; i < this.mySkills.length; i++) {
                let l = 0;
                for (let j = 0; j < this.dataSource.length; j++) {
                    if (this.mySkills[i].skillId === this.dataSource[j].skillId) {
                        l = 1;
                        // this.uniqueSkills[k] = this.mySkills[i];
                        // console.log(this.uniqueSkills[k]);
                        // k = k + 1;
                    }
                }
                if (l === 0) {
                this.uniqueSkills[k] = this.mySkills[i];
                k = k + 1;
                }
            }
            // console.log(this.uniqueSkills);
        });
    }
    openDialog(skillId): void {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.width = '25%';
        dialogConfig.data = { skillId };
        const dialogRef = this.dialog.open(SkillEditComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
            this.skillService.viewSkills().subscribe((response: any[]) => {
                // this.mySkills = response;
                this.ngOnInit();
            });
        });
    }
    onAdd() {
        this.skillService.saveData(this.newSkills.value).subscribe((response: any[]) => {
            this.skillService.viewSkills().subscribe((response: any[]) => {
                // this.mySkills = response;
            });
        });
        this.onReset();
    }
    onReset() {
        /* tslint:disable:no-string-literal */
        this.newSkills.controls['skillName'].setValue(['']);

    }
    onAddNewSkill(skillId, skillName) {
        this.skillnew = new SkillRatingNew();
        this.skillnew.skillId = skillId;
        this.skillnew.skillName = skillName;
        this.skillnew.rating = this.rating;
        this.skillnew.employeeId = this.employeeView.employeeId;
        // console.log(this.skillnew);
        this.skillService.saveDatanew(this.skillnew);
        this.dialogRef.close(false);
        // this.skillnew.controls['skillName'].setValue([skillName]);
        // this.skillnew.controls['skillId'].setValue([skillId]);
        // this.skillnew.controls['rating'].setValue([0]);
        // this.skillnew.controls['employeeId'].setValue([this.employeeView.employeeId]);
    }

    closeDialog() {
        this.dialogRef.close(false);
    }
}