import { TestBed, async } from '@angular/core/testing';
import { HomeTableComponent } from './home.component';
import { HeaderComponent } from '../header/header.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularMaterialModule } from '../shared/modules/angular-material/angular-material.module';
describe('HomeTableComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeTableComponent,
        HeaderComponent,
      ],
      imports: [AngularMaterialModule,
       RouterTestingModule, FormsModule ],
    }).compileComponents();
  }));

  it(`should have as text 'home page'`, async(() => {
    const fixture = TestBed.createComponent(HomeTableComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.text).toEqual('home page');
  }));
});