import { Component, Output, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {Subscription, timer} from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Employee } from '../model/employee';
import { EmployeeService } from '../services/employee.service';
import { SkillRating } from '../model/skillrating';
import { filter } from 'rxjs/operators';
import { HeaderComponent } from '../header/header.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeEditComponent } from '../employeeEdit/employeeEdit.component';
import { EmployeeViewComponent } from '../employeeView/employeeView.component';
import { OwnSkillComponent } from '../ownSkill/ownSkill.component';
import { TrackComponent } from '../track/track.component';
import { DialogService } from '../services/dialog.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})


export class HomeTableComponent implements OnInit {

  displayedColumns: string[] = ['status', 'name', 'Associate_Id', 'Email_Id', 'Mobile', 'Strong Skills', 'Change'];
  employeeList: any[];
  text = 'home page';
  total = 0;
  male = 0;
  female = 0;
  level1 = 0;
  level2 = 0;
  level3 = 0;
  css = 0;
  react = 0;
  java = 0;
  spring = 0;
  l = 2;
  rest = 0;
  html = 0;
  angular1 = 0;
  angular2 = 0;
  hello: any[];
  mode = 'determinate';
  dataSource1: any;
  chartConfig: any;
  searchKey: any;
  listData: MatTableDataSource <any[]>;
  // dataSource: any[];
  public skillList: any[];
  constructor(private router: Router,
              private employeeService: EmployeeService,
              public dialog: MatDialog,
              private dialogService: DialogService) {
    this.chartConfig = {
      width: '700',
      height: '400',
      type: 'pie2D',
      dataFormat: 'json',
    };
  }

  ngOnInit() {
    this.employeeService.getData().subscribe((res: any) => {
      /* tslint:disable:prefer-for-of */
      for (let i = 0; i < res.length; i++) {
        this.total = this.total + 1;
      }
      this.employeeList = res;
      // console.log(this.employeeList);
      this.listData = new MatTableDataSource(res);
      /* tslint:disable:prefer-for-of */
      for (let i = 0; i < res.length; i++) {
        const sk = res[i].skillRating;
        if (res[i].mygender === 'Male') {
          this.male = this.male + 1;
        }
        if (res[i].mygender === 'Female') {
          this.female = this.female + 1;
        }
        if (res[i].employeeLevel1 === true) {
          this.level1 = this.level1 + 1;
        }
        if (res[i].employeeLevel2 === true) {
          this.level2 = this.level2 + 1;
        }
        if (res[i].employeeLevel3 === true) {
          this.level3 = this.level3 + 1;
        }
        for (let j = 0; j < sk.length; j++) {
          if (sk[j].skillName === 'css3') {
            this.css = this.css + 1;
          }
          if (sk[j].skillName === 'html5') {
            this.html = this.html + 1;
          }
          if (sk[j].skillName === 'Java') {
            this.java = this.java + 1;
          }
          if (sk[j].skillName === 'Angular 1') {
            this.angular1 = this.angular1 + 1;
          }
          if (sk[j].skillName === 'Angular 2') {
            this.angular2 = this.angular2 + 1;
          }
          if (sk[j].skillName === 'React') {
            this.react = this.react + 1;
          }
          if (sk[j].skillName === 'spring boot') {
            this.spring = this.spring + 1;
          }
          if (sk[j].skillName === 'Spring Restful') {
            this.rest = this.rest + 1;
          }
        }
      }
      // this.spring = ((this.spring * 100) / this.total ) ;
      this.dataSource1 = {
        chart: {
          caption: 'Skill Representation',
          theme: 'fusion',
        },
        data: [{
          label: 'HTML',
          value: this.html
        }, {
          label: 'CSS',
          value: this.css
        }, {
          label: 'React',
          value: this.react
        }, {
          label: 'Spring Restful',
          value: this.rest
        }, {
          label: 'JAVA',
          value: this.java
        }, {
          label: 'Angular 1',
          value: this.angular1
        }, {
          label: 'Angular 2',
          value: this.angular2
        }, {
          label: 'Spring boot',
          value: this.spring
        }]
      };
      return res;
    });
    // this.dataSource = this.employeeList;
  }
  addSkill() {
    this.router.navigate(['add']);
    this.ngOnInit();
  }
  onSearchClear() {
    this.searchKey = ' ';
    this.applyFilter();
  }

  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  onTrack(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.height = '70%';
    const dialogRef = this.dialog.open(TrackComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  onEdit(employeeId): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '80%';
    dialogConfig.height = '80%';
    dialogConfig.data = { employeeId };
    const dialogRef = this.dialog.open(EmployeeEditComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
          this.total = 0;
          this.male = 0;
          this.female = 0;
          this.level1 = 0;
          this.level2 = 0 ;
          this.level3 = 0 ;
          this.ngOnInit();
    });
  }
  onView(employeeId): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.height = '70%';
    dialogConfig.data = { employeeId };
    const dialogRef = this.dialog.open(EmployeeViewComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  newSkill(employeeId): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = false;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '50%';
    dialogConfig.height = '70%';
    dialogConfig.data = { employeeId };
    const dialogRef = this.dialog.open(OwnSkillComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
    });
  }
  onDelete(employeeId) {
    this.dialogService.openConfirmDialog('Are you sure to delete this Employee?')
      .afterClosed().subscribe(res => {
        if (res) {
          this.employeeService.deleteEmployee(employeeId).subscribe((response) => {
          this.total = 0;
          this.male = 0;
          this.female = 0;
          this.level1 = 0;
          this.level2 = 0 ;
          this.level3 = 0 ;
          this.ngOnInit();
          });
          // location.reload();
        }
      });

  }

}
