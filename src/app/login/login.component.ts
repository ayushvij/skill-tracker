import { Component } from '@angular/core';
import { Router } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { LoginService } from '../services/login.service';
import { AuthService } from '../services/auth.service';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { LoginDetails } from '../model/loginDetails';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
// import { WrongLoginComponent } from '../wrongLogin/wrongLogin.component';




@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})


export class LoginComponent {
    constructor(private router: Router, private loginService: LoginService, private auth: AuthService, public dialog: MatDialog) { }
    title = 'EncryptionDecryptionSample';
    newEmployee = new FormGroup({
        employeeId: new FormControl(''),
        password: new FormControl(''),
    });
    public login: LoginDetails[] = [];
    employeeId: any;
    right =  0;
    employeeId1: any;
    employeeId2: any;
    password: any;
    password1: any;
    employeeId3: string;
    conversionEncryptOutput: any;
    conversionEncryptOutput1: string;
    plainText: string;
    encryptText: string;
    encPassword: string;
    decPassword: string;
    conversionDecryptOutput: string;
    conversionDecryptOutput1: string;
    onRegister() {
        this.router.navigate(['register']);
    }
    onLogin() {
        this.employeeId1 = this.employeeId.toString();
        this.conversionEncryptOutput = CryptoJS.AES.encrypt(this.employeeId1.trim(), this.password.trim()).toString();
        this.conversionDecryptOutput = CryptoJS.AES.decrypt(this.conversionEncryptOutput.trim(),
        this.password.trim()).toString(CryptoJS.enc.Utf8);
        // console.log(this.conversionDecryptOutput);
        this.loginService.findLogins(this.employeeId).subscribe((response: LoginDetails) => {
            this.password1 = response.password;
            this.employeeId2 = response.employeeId;
            this.employeeId3 = this.employeeId2.toString();
            this.conversionDecryptOutput1 = CryptoJS.AES.decrypt(this.password1.trim(),
                this.password.trim()).toString(CryptoJS.enc.Utf8);
            if (this.conversionDecryptOutput1 === this.employeeId1) {
                this.router.navigate(['home']);
                this.auth.setLoggedIn(true);
            } else {
                this.right = 1;
            }
        });
    }
}
