import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { RouterTestingModule } from '@angular/router/testing';
import { MatConfirmDialogComponent } from './mat-confirm-dialog.component';
import { MatDialogRef } from '@angular/material';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AngularMaterialModule } from '../shared/modules/angular-material/angular-material.module';

describe('MatConfirmDialogComponent', () => {
  let component: MatConfirmDialogComponent;
  let fixture: ComponentFixture<MatConfirmDialogComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatConfirmDialogComponent,
       ],
       imports: [AngularMaterialModule,
        ],
        providers: [{ provide: MatDialogRef, useValue: {} }, { provide: MAT_DIALOG_DATA, useValue: [] },
]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(MatConfirmDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
  it(`should have as title 'confirmation'`, () => {
    const fixture = TestBed.createComponent(MatConfirmDialogComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.text).toEqual('confirmation');
  });
});