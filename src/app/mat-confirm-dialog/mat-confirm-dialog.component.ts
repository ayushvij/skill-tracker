import { Component, OnInit, Inject } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
@Component({
    selector: 'app-mat-confirm-dialog',
    templateUrl: './mat-confirm-dialog.component.html',
    styleUrls: ['./mat-confirm-dialog.component.css']
})
export class MatConfirmDialogComponent implements OnInit {
    text = 'confirmation';
    someValue: boolean = false;
    constructor( @Inject(MAT_DIALOG_DATA) public data,
                 public dialogRef: MatDialogRef<MatConfirmDialogComponent>) { }
    ngOnInit() {
    }

    closeDialog() {
        this.dialogRef.close(this.someValue);
    }

}