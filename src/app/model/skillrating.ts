export class SkillRating {
  skillName: string;
  skillId: number;
  rating: number;
}
