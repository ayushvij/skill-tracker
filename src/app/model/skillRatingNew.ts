export class SkillRatingNew {
  skillName: string;
  skillId: number;
  rating: number;
  employeeId: number;
}
