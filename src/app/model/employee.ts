import { SkillRating } from './skillrating';
export class Employee {
employeeName: string;
employeeId: number;
employeeEmail: string;
employeeNumber: number;
employeeLevel1: boolean;
employeeLevel2: boolean;
employeeLevel3: boolean;
employeeStatusGreen: boolean;
employeeStatusBlue: boolean;
employeeStatusRed: boolean;
employeeRemark: string;
employeeStrength: string;
employeeWeakness: string;
other: string;
mygender: string;
skillRating: SkillRating[];
}
