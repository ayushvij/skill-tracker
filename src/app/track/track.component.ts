import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from '../services/employee.service';
import { SkillService } from '../services/skill.service';
import { SkillRating } from '../model/skillrating';
import { Employee } from '../model/employee';
@Component({
    selector: 'app-track-view',
    templateUrl: './track.component.html',
    styleUrls: ['./track.component.css']
})


export class TrackComponent implements OnInit {
    total = 0;
    css = 0;
    react = 0;
    java = 0;
    spring = 0;
    l = 2;
    rest = 0;
    html = 0;
    angular1 = 0;
    angular2 = 0;
    dataSource1: any;
    chartConfig: any;
    constructor(private router: Router,
                private employeeService: EmployeeService,
                public dialogRef: MatDialogRef <TrackComponent >,
                private skillService: SkillService, @Inject(MAT_DIALOG_DATA) public data: any) {
        this.chartConfig = {
             width: '90%'  ,
             height: '100%',
            type: 'pie2D',
            dataFormat: 'json',
        };
        this.employeeService.getData().subscribe((res: any) => {
            // for (const ress of res)
            // {
            //  this.total = this.total + 1;
            // }
            // console.log(this.total);
            // for (let i = 0; i < res.length; i++) {
            //     this.total = this.total + 1;
            // }
            // console.log(this.employeeList);
            /* tslint:disable:prefer-for-of */
            for (let i = 0; i < res.length; i++) {
                const sk = res[i].skillRating;
                for (let j = 0; j < sk.length; j++) {
                    if (sk[j].skillName === 'css3') {
                        this.css = this.css + 1;
                    }
                    if (sk[j].skillName === 'html5') {
                        this.html = this.html + 1;
                    }
                    if (sk[j].skillName === 'Java') {
                        this.java = this.java + 1;
                    }
                    if (sk[j].skillName === 'Angular 1') {
                        this.angular1 = this.angular1 + 1;
                    }
                    if (sk[j].skillName === 'Angular 2') {
                        this.angular2 = this.angular2 + 1;
                    }
                    if (sk[j].skillName === 'React') {
                        this.react = this.react + 1;
                    }
                    if (sk[j].skillName === 'spring boot') {
                        this.spring = this.spring + 1;
                    }
                    if (sk[j].skillName === 'Spring Restful') {
                        this.rest = this.rest + 1;
                    }
                }

            }

            // this.spring = ((this.spring * 100) / this.total ) ;
            this.dataSource1 = {
                chart: {
                    caption: 'Skill Representation',
                    theme: 'fusion',
                },
                data: [{
                    label: 'HTML',
                    value: this.html
                }, {
                    label: 'CSS',
                    value: this.css
                }, {
                    label: 'React',
                    value: this.react
                }, {
                    label: 'Spring Restful',
                    value: this.rest
                }, {
                    label: 'JAVA',
                    value: this.java
                }, {
                    label: 'Angular 1',
                    value: this.angular1
                }, {
                    label: 'Angular 2',
                    value: this.angular2
                }, {
                    label: 'Spring boot',
                    value: this.spring
                }]
            };
            return res;
        });
    }
    ngOnInit() {
        // this.dataSource = this.employeeList;
    }

closeDialog() {
    this.dialogRef.close(false);
}
}

