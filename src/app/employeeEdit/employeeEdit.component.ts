import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { EmployeeService } from '../services/employee.service';
import { SkillService } from '../services/skill.service';
import { SkillRating } from '../model/skillrating';
import { Employee } from '../model/employee';
@Component({
    selector: 'app-employee-edit',
    templateUrl: './employeeEdit.component.html',
    styleUrls: ['./employeEdit.component.css'],
})


export class EmployeeEditComponent implements OnInit {
     myEmployee: Employee;
    mySkills: any[];
    gend: string;
    ratedSkills: SkillRating[] = [];
    gender: string[] = ['Male', 'Female', 'Other'];
    skillRating: SkillRating[] = [];
    max = 20;
    min = 0;
    thumbLabel = true;
    value = 0;
    constructor(private router: Router,
                private employeeService: EmployeeService,
                public dialogRef: MatDialogRef<EmployeeEditComponent >,
                private skillService: SkillService, @Inject(MAT_DIALOG_DATA) public data: any) { }
    newEmployeeEdit = new FormGroup({
        employeeName: new FormControl({ disabled: true} ),
        employeeId: new FormControl(''),
        employeeEmail: new FormControl(''),
        employeeNumber: new FormControl(''),
        employeeLevel1: new FormControl(''),
        employeeLevel2: new FormControl(''),
        employeeLevel3: new FormControl(''),
        employeeStatusGreen: new FormControl(''),
        employeeStatusRed: new FormControl(''),
        employeeStatusBlue: new FormControl(''),
        employeeRemark: new FormControl(''),
        employeeStrength: new FormControl(''),
        employeeWeakness: new FormControl(''),
        other: new FormControl(''),
    });
    ngOnInit() {
        this.skillService.viewSkills().subscribe((response1: any[]) => {
            this.mySkills = response1;
        });
        this.employeeService.findEmployeebyID(this.data.employeeId).subscribe((response: Employee) => {
             /* tslint:disable:no-string-literal */
            this.newEmployeeEdit.controls['employeeId'].setValue(response['employeeId']);
            this.newEmployeeEdit.controls['employeeName'].setValue(response['employeeName']);
            this.newEmployeeEdit.controls['employeeEmail'].setValue(response['employeeEmail']);
            this.newEmployeeEdit.controls['employeeNumber'].setValue(response['employeeNumber']);
            this.newEmployeeEdit.controls['employeeRemark'].setValue(response['employeeRemark']);
            this.newEmployeeEdit.controls['employeeStatusBlue'].setValue(response['employeeStatusBlue']);
            this.newEmployeeEdit.controls['employeeStatusGreen'].setValue(response['employeeStatusGreen']);
            this.newEmployeeEdit.controls['employeeStatusRed'].setValue(response['employeeStatusRed']);
            this.newEmployeeEdit.controls['employeeStrength'].setValue(response['employeeStrength']);
            this.newEmployeeEdit.controls['employeeWeakness'].setValue(response['employeeWeakness']);
            this.newEmployeeEdit.controls['employeeLevel1'].setValue(response['employeeLevel1']);
            this.newEmployeeEdit.controls['employeeLevel2'].setValue(response['employeeLevel2']);
            this.newEmployeeEdit.controls['employeeLevel3'].setValue(response['employeeLevel3']);
            this.gend = response.mygender;
            this.newEmployeeEdit.controls['other'].setValue(response['other']);
            /* tslint:disable:no-string-literal */
            this.ratedSkills = response.skillRating;
        });
    }
    onSubmit(skillId) {
        this.myEmployee = new Employee();
        this.myEmployee.employeeName = this.newEmployeeEdit.get('employeeName').value;
        this.myEmployee.employeeId = this.newEmployeeEdit.get('employeeId').value;
        this.myEmployee.employeeEmail = this.newEmployeeEdit.get('employeeEmail').value;
        this.myEmployee.employeeNumber = this.newEmployeeEdit.get('employeeNumber').value;
        this.myEmployee.employeeLevel1 = this.newEmployeeEdit.get('employeeLevel1').value;
        this.myEmployee.employeeLevel2 = this.newEmployeeEdit.get('employeeLevel2').value;
        this.myEmployee.employeeLevel3 = this.newEmployeeEdit.get('employeeLevel3').value;
        this.myEmployee.employeeStatusGreen = this.newEmployeeEdit.get('employeeStatusGreen').value;
        this.myEmployee.employeeStatusBlue = this.newEmployeeEdit.get('employeeStatusBlue').value;
        this.myEmployee.employeeStatusRed = this.newEmployeeEdit.get('employeeStatusRed').value;
        this.myEmployee.employeeStrength = this.newEmployeeEdit.get('employeeStrength').value;
        this.myEmployee.employeeRemark = this.newEmployeeEdit.get('employeeRemark').value;
        this.myEmployee.employeeWeakness = this.newEmployeeEdit.get('employeeWeakness').value;
        this.myEmployee.other = this.newEmployeeEdit.get('other').value;
        const mySkillsfresh = [];
        for (let i = 0, k = 0; i < this.ratedSkills.length; i++) {
            if ((this.ratedSkills[i].rating) !== 0) {
                mySkillsfresh[k] = this.ratedSkills[i];
                k++;
            }
        }
        this.myEmployee.skillRating = mySkillsfresh;
        this.myEmployee.mygender = this.gend;
        this.employeeService.saveData(this.myEmployee);
        this.dialogRef.close(false);
    }
    newSkill(employeeId) {
    this.router.navigate(['newSkill'], employeeId);
    this.dialogRef.close(false);
    }
    closeDialog() {
        this.dialogRef.close(false);
    }
}
